﻿#include <iostream>

using namespace std;

class Vector {
private:
	double x, y, z;
public:
	Vector(double x, double y, double z);
	double getX();
	double getY();
	double getZ();
	double getVectorLength();
};
Vector::Vector(double x, double y, double z) {
	this->x = x;
	this->y = y;
	this->z = z;
}
double Vector::getX() {
	return x;
}
double Vector::getY() {
	return y;
}
double Vector::getZ() {
	return z;
}


double Vector::getVectorLength() {
	return sqrt(x * x + y * y + z * z);
}

int main() {
	Vector vector(3, 4, 7);
	cout << "x: " << vector.getX() << endl;
	cout << "y: " << vector.getY() << endl;
	cout << "z: " << vector.getZ() << endl;
	cout << "length: " << vector.getVectorLength() << endl;
	return 0;
}